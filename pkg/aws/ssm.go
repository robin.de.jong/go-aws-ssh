package aws

import (
	"context"
	"log"
	"strings"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/secretsmanager"
)

type SecretManager struct {
	client *secretsmanager.Client
}

func NewSecretManager(ctx context.Context) *SecretManager {
	client := SecretManager{}
	client.setAWSClient(ctx, "us-east-1")
	return &client
}

func (s *SecretManager) GetSeceret(name string, ctx context.Context) string {
	if !strings.HasPrefix(name, ".pem") {
		name += ".pem"
	}
	input := secretsmanager.GetSecretValueInput{
		SecretId: aws.String(name),
	}

	result, err := s.client.GetSecretValue(ctx, &input)
	if err != nil {
		log.Fatal("Error getting secret value, with error:", err)
	}

	return *result.SecretString
}

func (s *SecretManager) setAWSClient(ctx context.Context, region string) {
	cfg, err := config.LoadDefaultConfig(ctx,
		config.WithRegion(region),
	)

	if err != nil {
		log.Fatalln("Error setting the config: ", err)
	}

	s.client = secretsmanager.NewFromConfig(cfg)
}
