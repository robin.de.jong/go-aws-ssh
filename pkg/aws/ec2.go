package aws

import (
	"context"
	"log"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/aws/aws-sdk-go-v2/service/ec2/types"
)

type EC2 struct {
	client *ec2.Client

	describeInstance map[string]*ec2.DescribeInstancesOutput
}

type EC2Instance struct {
	InstanceId string
	PrivateIp  string
	PublicIp   string
	KeyName    string
	Region     string
}

func NewEC2Client(ctx context.Context) *EC2 {
	client := EC2{}
	client.setAWSClient(ctx, "us-east-1")
	return &client
}

func (e *EC2) FindInstance(instance string, regions []string, ctx context.Context) *EC2Instance {
	filters := make(map[string]string)
	filters["InstanceId"] = instance
	ec2Instances := []EC2Instance{}

	for _, region := range regions {
		e.setAWSClient(ctx, region)
		instances, err := e.filterInstances(ctx, region, filters)
		if err != nil {
			log.Fatalln("Error finding instances", err)
		}

		for _, instance := range instances {
			ec2Instances = append(ec2Instances, EC2Instance{
				InstanceId: *instance.InstanceId,
				PrivateIp:  *instance.PrivateIpAddress,
				PublicIp:   *instance.PublicIpAddress,
				KeyName:    *instance.KeyName,
				Region:     region,
			})
		}

	}
	if len(ec2Instances) > 0 {
		return &ec2Instances[0]
	} else {
		return nil
	}
}

func (e *EC2) instancesInRegion(region string) bool {
	ctx := context.TODO()
	e.setAWSClient(ctx, region)
	input := ec2.DescribeInstancesInput{}
	result, err := e.client.DescribeInstances(ctx, &input)
	if err != nil {
		log.Fatalln("Error finding EC2 instances in", region, err)
		return false
	}
	return len(result.Reservations) > 0
}

func (e *EC2) GetAvailableRegions() []string {
	input := ec2.DescribeRegionsInput{}
	result, err := e.client.DescribeRegions(context.TODO(), &input)

	if err != nil {
		log.Fatalln("Error finding all regions:", err)
	}

	regions := []string{}
	for _, region := range result.Regions {
		name := *region.RegionName

		if e.instancesInRegion(name) {
			log.Println("There are instances running in region", name)
			regions = append(regions, name)
		} else {
			log.Println("There are NO instances running in region", name)
		}
	}

	return regions
}

func (e *EC2) filterInstances(ctx context.Context, region string, filters map[string]string) ([]types.Instance, error) {
	input := ec2.DescribeInstancesInput{}

	if e.describeInstance == nil {
		e.describeInstance = make(map[string]*ec2.DescribeInstancesOutput)
	}

	result, ok := e.describeInstance[region]
	if !ok {
		var err error
		result, err = e.client.DescribeInstances(ctx, &input)
		if err != nil {
			return nil, err
		}
		e.describeInstance[region] = result
	}

	keys := e.keysAsSlice(filters)

	instances := []types.Instance{}

	for _, reservation := range result.Reservations {
		for _, instance := range reservation.Instances {
			fields := FindFields(
				instance,
				keys,
			)

			if e.valueContains(filters, fields) {
				instances = append(instances, instance)
			}
		}
	}

	return instances, nil
}

func (e *EC2) valueContains(filters map[string]string, fields []Field) bool {
	for _, field := range fields {
		if filters[field.Name] == field.Value {
			return true
		}
	}

	return false
}

func (e *EC2) keysAsSlice(filters map[string]string) []string {
	keys := []string{}
	for key := range filters {
		keys = append(keys, key)
	}
	return keys
}

func (e *EC2) findInstanceWithValue(value string, regions []string) []EC2Instance {
	filters := make(map[string]string)
	filters["PrivateIpAddress"] = value
	filters["PublicIpAddress"] = value
	filters["PrivateDnsName"] = value
	filters["PublicDnsName"] = value

	ctx := context.TODO()
	ec2Instances := []EC2Instance{}

	for _, region := range regions {
		e.setAWSClient(ctx, region)
		instances, err := e.filterInstances(ctx, region, filters)
		if err != nil {
			log.Fatalln("Error finding instances", err)
		}

		for _, instance := range instances {
			ec2Instances = append(ec2Instances, EC2Instance{
				InstanceId: *instance.InstanceId,
				Region:     region,
			})
		}

	}
	return ec2Instances
}

func (e *EC2) setAWSClient(ctx context.Context, region string) {
	cfg, err := config.LoadDefaultConfig(ctx,
		config.WithRegion(region),
	)

	if err != nil {
		log.Fatalln("Error setting the config: ", err)
	}

	e.client = ec2.NewFromConfig(cfg)
}
