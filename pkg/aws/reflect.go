package aws

import (
	"reflect"
)

type Field struct {
	Name  string
	Kind  string
	Value string
}

func FindAllFields(obj interface{}) []Field {
	fields := []Field{}

	val := reflect.TypeOf(obj)

	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)
		switch field.Type.Kind() {
		case reflect.Struct:
			// fields = append(fields, FindAllFields(field)...)
		case reflect.Array, reflect.Slice:
			// for j := 0; j < field.Len(); j++ {
			// fields = append(fields, FindAllFields(field.Index(j).Interface())...)
			// }
		default:
			fields = append(fields, Field{
				Name:  field.Name,
				Kind:  field.Type.Kind().String(),
				Value: GetValueFromAttribute(field, obj),
			})
		}
	}

	return fields
}

func GetValueFromAttribute(field reflect.StructField, obj interface{}) string {
	value := reflect.ValueOf(obj)
	if value.Kind() == reflect.Ptr {
		value = value.Elem()
	}
	objField := value.FieldByName(field.Name)
	if objField.Kind() == reflect.Ptr {
		objField = objField.Elem()
	}
	return objField.String()
}

func FindFields(obj interface{}, attributes []string) []Field {
	allFields := FindAllFields(obj)
	fields := []Field{}

	for _, field := range allFields {
		if arrayContains(field.Name, attributes) {
			fields = append(fields, field)
		}
	}

	return fields
}

func arrayContains(key string, arr []string) bool {
	for _, obj := range arr {
		if key == obj {
			return true
		}
	}
	return false
}
