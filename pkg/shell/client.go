package shell

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"
)

type Session struct {
	// sshSession    *ssh.Session
	sshConnection     *ssh.Client
	bastionConnection *ssh.Client

	// stdin io.WriteCloser
}

type BastionConfig struct {
	Username   string
	PrivateKey string
}

func NewShellSession(user string, key string, ip string) (*Session, error) {
	s := Session{}
	var err error
	s.sshConnection, err = s.connect(user, key, ip)
	if err != nil {
		return nil, err
	}

	return &s, nil
}

func NewBastionShellSession(user string, key string, ip string, cfg BastionConfig) (*Session, error) {
	s := Session{}
	var err error
	s.sshConnection, err = s.bastionConnect(user, key, ip, cfg)
	if err != nil {
		return nil, err
	}

	return &s, nil
}

func GetPrivateKey(key string) string {
	home, _ := os.UserHomeDir()
	keyFile, err := os.ReadFile(fmt.Sprint(home, "/.ssh/", key))
	if err != nil {
		return ""
	}
	return string(keyFile)
}

func (s *Session) getBastion() string {
	bastion := "bastion.us.readyeducation.com"
	return fmt.Sprint(bastion, ":22")
}

func (s *Session) sanatizeKey(data string) []byte {
	str := string(data)
	return []byte(str)
}

func (s *Session) absolutePath(path string) string {
	home, _ := os.UserHomeDir()
	return strings.ReplaceAll(path, "~", home)
}

func (s *Session) bastionConnect(user string, key string, ip string, bastionConfig BastionConfig) (*ssh.Client, error) {
	privateKey, err := ssh.ParsePrivateKey(
		s.sanatizeKey(key),
	)
	if err != nil {
		return nil, err
	}

	privateFile, err := os.ReadFile(s.absolutePath(bastionConfig.PrivateKey))
	if err != nil {
		return nil, err
	}
	privateBastionKey, err := ssh.ParsePrivateKey(
		s.sanatizeKey(string(privateFile)),
	)
	if err != nil {
		return nil, err
	}

	bastionClientConfig := &ssh.ClientConfig{
		User: bastionConfig.Username,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(privateBastionKey),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         30 * time.Second,
	}

	cfg := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(privateKey),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         30 * time.Second,
	}

	bastionConn, err := ssh.Dial("tcp", s.getBastion(), bastionClientConfig)
	s.bastionConnection = bastionConn
	if err != nil {
		return nil, err
	}

	addr := fmt.Sprint(ip, ":22")
	conn, err := s.bastionConnection.Dial("tcp", addr)
	if err != nil {
		return nil, err
	}

	ncc, chans, reqs, err := ssh.NewClientConn(conn, addr, cfg)
	if err != nil {
		return nil, err
	}

	client := ssh.NewClient(ncc, chans, reqs)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func (s *Session) connect(user string, key string, ip string) (*ssh.Client, error) {
	privateKey, err := ssh.ParsePrivateKey(
		s.sanatizeKey(key),
	)
	if err != nil {
		return nil, err
	}

	cfg := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(privateKey),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         30 * time.Second,
	}

	addr := fmt.Sprint(ip, ":22")
	conn, err := ssh.Dial("tcp", addr, cfg)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func (s *Session) Run(cmd string) string {
	session, err := s.sshConnection.NewSession()
	if err != nil {
		log.Fatalln("Received error while opening session:", err)
		return ""
	}
	defer session.Close()

	var outPipe bytes.Buffer
	session.Stdout = &outPipe

	err = session.Run(cmd)
	if err != nil {
		log.Println("Received error:", err)
		return ""
	}

	return outPipe.String()
}

func (s *Session) Close() {
	s.sshConnection.Close()

	if s.bastionConnection != nil {
		s.bastionConnection.Close()
	}
}
