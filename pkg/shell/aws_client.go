package shell

import (
	"cert-renewal/pkg/aws"
	"context"
	"log"
)

func NewAwsShell(user string, instance string, bastionConfig BastionConfig) (*Session, error) {
	ctx := context.Background()
	awsClient := aws.NewEC2Client(ctx)
	secret := aws.NewSecretManager(ctx)
	// regions := ec2.GetAvailableRegions()

	ec2Instance := awsClient.FindInstance(instance, []string{"us-east-1"}, ctx)
	key := secret.GetSeceret(ec2Instance.KeyName, ctx)

	session, err := NewBastionShellSession("centos", key, ec2Instance.PrivateIp, bastionConfig)
	if err != nil {
		log.Fatal("Error opening shell, with error:", err)
	}

	return session, nil
}
