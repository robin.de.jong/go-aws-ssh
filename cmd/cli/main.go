package main

import (
	"cert-renewal/pkg/shell"
	"log"
)

func main() {
	bastionCfg := shell.BastionConfig{
		Username:   "robin_de_jong",
		PrivateKey: "~/.ssh/id_ed25519",
	}

	session, err := shell.NewAwsShell("centos", "i-084c78c4fec69e15b", bastionCfg)
	// key := shell.GetPrivateKey(bastionCfg.PrivateKey)
	// session, err := shell.NewShellSession("robin", key, "jemoeders.website")
	if err != nil {
		log.Fatal("Error opening shell, with error:", err)
	}

	out := session.Run("uname -a")
	log.Println(out)
	out = session.Run("ls -l")
	log.Println(out)

	defer session.Close()
}
